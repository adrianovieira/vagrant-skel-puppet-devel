'''
/**
 * Vagrant base for puppet devel and testes
 *
 * @author Adriano Vieira <adriano.svieira at gmail.com>
 * @license @see LICENCE
 */
 '''

# VM NAME on virtualbox (default: vgvm-puppet-dev_tests)
VGVM_NAME = ENV.key?('VGVM_NAME') ? ENV['VGVM_NAME'] : "vgvm-node-dev-tests"

# VM memory on virtualbox (default: 1024MB)
VGVM_MEMORY = ENV.key?('VGVM_MEMORY') ? ENV['VGVM_MEMORY'] : 1024

# VM cpu/cores on virtualbox (default: 1 cpu/core)
VGVM_CPU = ENV.key?('VGVM_CPU') ? ENV['VGVM_CPU'] : 1

# VM domain suffix (default: hacklab)
VGVM_DOMAIN = ENV.key?('VGVM_DOMAIN') ? ENV['VGVM_DOMAIN'] : "hacklab"

# VM private network (default: 10.0.3.10)
VGVM_PVTNET = ENV.key?('VGVM_PVTNET') ? ENV['VGVM_PVTNET'] : "10.0.3.10"

# VM puppet environment (default: production)
VGVM_PP_ENVIRONMENT = ENV.key?('VGVM_PP_ENVIRONMENT') ? ENV['VGVM_PP_ENVIRONMENT'] : "production"

# VM puppet adicional options (default: --verbose)
VGVM_PP_OPTIONS = ENV.key?('VGVM_PP_OPTIONS') ? ENV['VGVM_PP_OPTIONS'] : "--verbose"

# VM fqdn
VGVM_FQDN = "#{VGVM_NAME}.#{VGVM_DOMAIN}"

# vagrant-proxyconf: https://tmatilai.github.io/vagrant-proxyconf
# if necessary set OS environment variable PROXY|HTTP_PROXY|HTTPS_PROXY="http://proxy:port"
if ENV.key?('PROXY')
  HTTP_PROXY=ENV['PROXY']
elsif ENV.key?('proxy')
  HTTP_PROXY=ENV['proxy']
elsif ENV.key?('HTTP_PROXY')
  HTTP_PROXY = ENV['HTTP_PROXY']
elsif ENV.key?('http_proxy')
  HTTP_PROXY = ENV['http_proxy']
elsif ENV.key?('HTTPS_PROXY')
  HTTP_PROXY = ENV['HTTPS_PROXY']
elsif ENV.key?('https_proxy')
  HTTP_PROXY = ENV['https_proxy']
else
  print Vagrant.has_plugin?("vagrant-proxyconf") ? "WARN: you installed vagrant-proxyconf plugin, but proxy environment variable (PROXY|HTTP_PROXY|HTTPS_PROXY) not set yet!\n\n" : ''
  HTTP_PROXY="http://proxy_not_set:3128"
end
HTTPS_PROXY=HTTP_PROXY

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "puppetlabs/centos-7.2-64-puppet"
  config.vm.box_check_update = false

  if Vagrant.has_plugin?("vagrant-proxyconf") && (HTTP_PROXY != "http://proxy_not_set:3128") # proxy settings
    config.proxy.http     = HTTP_PROXY
    config.proxy.https    = HTTPS_PROXY
    config.proxy.no_proxy = "localhost, 127.0.0.1, .#{VGVM_DOMAIN}"
    print "proxy settings: \n"
    print " - proxy.http:  "+config.proxy.http+"\n"
    print " - proxy.https: "+config.proxy.https+"\n"
    print " - no_proxy: "+config.proxy.no_proxy+"\n\n"
  end # end proxy settings

  config.vm.define "vgvm-node-dev-tests" do |node| # define-VM vagrant external name

    # plugin https://github.com/oscar-stack/vagrant-hosts
    node.vm.host_name = "#{VGVM_FQDN}" # if plugin installed also set /etc/hosts
		node.vm.network "private_network", ip: VGVM_PVTNET

    node.vm.provider "virtualbox" do |virtualbox| # Virtualbox.settings
      virtualbox.name = VGVM_FQDN
      virtualbox.memory = VGVM_MEMORY
      virtualbox.cpus = VGVM_CPU
    end # end Virtualbox.settings

    # puppet provider settings
    node.vm.provision "puppet-standalone", type: "puppet", :options => ["--pluginsync"] do |puppet|
      puppet.facter = {
        'exec_env' => 'vagrant',
        'developer_email' => `git config -z user.email`,
      }
      puppet.manifests_path = "provision"
      puppet.environment_path = "provision"
      puppet.module_path = "provision/modules"
      puppet.manifest_file = "site.pp"
      puppet.environment = "#{VGVM_PP_ENVIRONMENT}"
      puppet.options = "#{VGVM_PP_OPTIONS}"
      puppet.hiera_config_path = "provision/hiera_server.yaml"
    end # end puppet standalone settings

  end # end-of-define-VM

end # end-of-file
