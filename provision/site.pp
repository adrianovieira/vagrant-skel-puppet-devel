# VGVM Initial settings
#
# @file Site.pp
node default {
  if (!$developer_email) {
    fail('Developer email not defined for git development! Check "git config user.email" and set it up! ')
  }

  $packages = ['git', 'ruby', 'ruby-devel', 'docker', 'docker-registry']

  package { $packages:
    ensure => installed,
  }
  ->
  exec { 'gem_install_bundle':
    command => 'gem install bundle',
    path => '/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin:/home/vagrant/.local/bin:/home/vagrant/bin',
    onlyif => ["test ! `gem list bundle|grep bundle`"]
  }
  ->
  exec { 'bundle_install_Gemfile':
    command => "bundle install",
    cwd => '/vagrant/',
    path => '/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin:/home/vagrant/.local/bin:/home/vagrant/bin',
    environment => ["PUPPET_VERSION=${::puppetversion}"],
    onlyif => ["test ! `gem list rspec-puppet|grep rspec-puppet` || test ! `gem list serverspec|grep serverspec`"]
  }

  # vagrant facter: developer_email (it'd be a hiera)
  exec { 'ssh_keys_create':
    command => "/usr/bin/ssh-keygen  -t rsa -C '${developer_email}' -q -N '' -f /home/vagrant/.ssh/id_rsa",
    onlyif  => ["test ! -f /home/vagrant/.ssh/id_rsa"],
    path    => '/usr/bin/',
    user    => 'vagrant'
  }
  ->
  file { 'ssh_keys_pub_share':
    path   => "/vagrant/provision/id_rsa-${::hostname}.pub",
    source => "/home/vagrant/.ssh/id_rsa.pub",
  }

} # end node default
