# vagrant-skel-puppet-devel

Vagrant setup VM for puppet module development and automation tests

## Requiriments

**Install tools:**

Install these tools on your desktop

- [Vagrant](https://www.vagrantup.com/)
  - [*vagrant-proxyconf* (optional)](https://tmatilai.github.io/vagrant-proxyconf)
  - [*vagrant-hosts* (optional)](https://github.com/oscar-stack/vagrant-hosts)
- [VirtualBox](http://www.virtualbox.org/)
- [Git](https://git-scm.com/)

## What this setup has?

A provision for a VM with pre-installed gems as *rspec-puppet, serverspec, guard-rspec*.



*`to be improved`*
